const puppeteer = require('puppeteer');
const iwarehouse = require('./libs/insertMachine/insertmachine').InsertWarehouse;

(async () => {
    const browser = await puppeteer.launch({product: 'firefox', headless: false});
    const page = await browser.newPage();
    await page.goto('https://mail.online.inet.co.th/owa/#path=/mail', {waitUntil: 'load'})
    .then(async () => {
        await page.waitForSelector('input#username').then(async (el) => {
            el.click().then(async () => {
                page.keyboard.type('phoomin.s@comseven.com')
                .then(async () => {
                    await page.waitForSelector('input#password').then(async (el) => {
                        el.click().then(async () => {
                            page.keyboard.type('4fc9870f').then(async () => {
                                page.keyboard.press('Enter').then(async () => {
                                    getData(page);
                                }).catch(err => console.log(err));
                            })
                        }).catch(err => console.log(err));
                    }).catch(err => console.log(err)).catch(err => console.log(err));
                }).catch(err => console.log(err));
            }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    });
})();

async function getData(page) {
    let rawData = "";
    await page.waitForSelector('div#_ariaId_52').then(async (el) => {
        await el.click().then(async () => {
            await page.waitForSelector('div#_ariaId_27')
            .then(async (el) => {
                el.click().then(async () => {
                    await page.waitForSelector('div._rp_45')
                    .then(async () => {
                        await page.$$('li').then(async (els) => {
                            for (let i = 0;i < els.length;i++) {
                                let warehousedata = iwarehouse.format(await ( await els[i].getProperty('textContent')).jsonValue());
                            }
                        }).catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
                }).catch(err => console.log(err));
            })
            .catch(err => console.log(err))
        }).catch(err => err);
    }).catch(err => console.log(err));
}