var puppeteer = require('puppeteer');

class InsertMachine {
    constructor (type) {
        this.type = type;
        this.data;
    }

    getInsertMachine(type) {
        return new InsertMachine(type);
    }

    format(type) {

    }

    exec(data, type = "") {

    }
}

class InsertWarehouse extends InsertMachine {
    constructor() {
        super();
        this.type = 'warehouse'
        this.data = {
            key: [],
            word: []
        };
    }

    format(rawdata) {
        data = rawdata.split(/\s/m);
        console.log(data);
        this.data.key.push(data[0]);
        this.data.word.push(data[1]);

        return this;
    }
}

module.exports = {
    "InsertMachine" : new InsertMachine,
    "InsertWarehouse": new InsertWarehouse
}