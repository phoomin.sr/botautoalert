const sqlConnect = require("./sqlConnect");

class sqlCommand extends sqlConnect {
    constructor (sqlConfig) {
        super();
        this.config = sqlConfig;

        return this;
    }
}

module.exports = sqlCommand;