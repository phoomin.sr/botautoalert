DECLARE @yDate DATE;
SET @yDate = DATEADD(DAY, -2, GETDATE());
DECLARE @lDate DATE;
SET @lDate = DATEADD(DAY, -3, GETDATE());


SELECT * FROM (
SELECT DISTINCT Category.GroupCatID, Category.Detail, Product.CategoryID, Category.[Name], Product.Info8 AS SubCat
FROM [10.100.101.48].[SSSearch].[dbo].[Product] Product
JOIN [10.100.101.48].[SSSearch].[dbo].[Category] Category ON Category.ID = Product.CategoryID
WHERE CAST(Product.CrTime as date) >= @yDate

Except

SELECT DISTINCT Category.GroupCatID, Category.Detail, Product.CategoryID, Category.[Name], Product.Info8 AS SubCat
FROM [10.100.101.48].[SSSearch].[dbo].[Product] Product
JOIN [10.100.101.48].[SSSearch].[dbo].[Category] Category ON Category.ID = Product.CategoryID
WHERE YEAR(Product.CrTime) >= '2020' AND CAST(Product.CrTime as date) <= @lDate
) All_Cat_FROMITEC