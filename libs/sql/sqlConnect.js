const sql = require('mssql');
const fs = require('fs');

class sqlConnect {
    constructor () {
        this.config;
        this.result;
    }
    async Connect () {
        this.conn = await sql.connect(this.config)
        .catch(err => console.log(err));

        return this;
    }

    async queryFromfile(directory) {
        let query = fs.readFileSync(directory, 'utf8');

        return this.query(query);
    }

    async query(query) {
        await this.Connect();
        this.result = await this.conn.query(query);
        await this.conn.close();

        return this.result.recordset;
    }
}

module.exports = sqlConnect;