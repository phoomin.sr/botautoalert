const sql = require('mssql');
const workbook = require('exceljs');
const wb = new workbook.Workbook();
wb.creator = "autobot";
wb.lastModifiedBy = 'autobot';
wb.created = new Date();
wb.modified = new Date();
const configuration = require('./configuration').sqlConfig['srv-dev108-PHOOMIN-MIS'];
const configuration_Temp = require('./configuration').sqlConfig['srv-dynamicsAX-MISDEV'];
const importTemps = require('./libs/line_chatbot/import_data_templates.json');
const { LineClient } = require('messaging-api-line');
const sqlCommand = require('./libs/sql/sqlCommand');
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly'];
const TOKEN_PATH = './libs/googleapis/token/token.json';



const client = new LineClient({
    accessToken: 'fDlox1shrxTaCZJnFaNl55aVNDh/M1fxAL59kJ/a3ZI3ATi5EU1fu5jKJvLRQMGB0ffLFAzhQ6uOY7Jqy2MprwtWXr7pCbJ7fTfeuZ9CNHG/nHz+4RwyfccMyXeI8gas2XJSmoEK0DE9NGC5paKeZwdB04t89/1O/w1cDnyilFU=',
    channelSecret: '104a6fa81b58e4eb79ecc51fc0dd0230'
});

// const userID = 'Ua0c2a3546f00c92b931fe127b7d30220';
const userID = 'Cd76eddb9d5616b7bab1c3945d8f28afc';
const cron = require('node-cron');


const sqlConfig = {
    user: 'MISDEV',
    password: '0608H',
    database: 'ITECToAX',
    server: '43.254.133.155',
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000
    },
    options: {
      encrypt: false, // for azure
      trustServerCertificate: false // change to true for local dev / self-signed certs
    }
};

function strDate (date) {
    let d = new Date(date);
    return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

async function check_daily_import () {
    let message = [];
    const sqlx = new sqlCommand(configuration);
    const data = await sqlx.query(`
    DECLARE @importTable TABLE (
        TableName NVARCHAR (MAX),
        ImportTime DATE,
        [Rows] INT DEFAULT 0
    )

	INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'SO' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[Invoice]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date) 

	INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'CN' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[CreditNote]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)

	INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'Receipt Inv.' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[Receipt]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)
    
    INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'Receipt Dep.' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[ReceiptDeposit]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)

	INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'Customer' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[Customer]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)

	INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'Product' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[Product]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)

	INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'PO' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[PO]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)
    
    INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'PO Receipt' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[Buy]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)
    
    INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'Deposit' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[Deposit]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)
    
    INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'InventoryInOut' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[InventoryInOut]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)
    
    INSERT INTO @importTable (TableName, ImportTime, [Rows])
    SELECT DISTINCT 'Petty Cash' tableName, CAST(ImportTime as date) ImportTime, COUNT(*) [Rows] 
    FROM [ITECToAX].[dbo].[PettyCash]
    WHERE CAST(ImportTime as date) = CAST(DATEADD(DAY, -1, GETDATE()) as date)
    GROUP BY CAST(ImportTime as date)
    
    SELECT * FROM @importTable`);

    for (let i = 0;i < data.length;i++) {
        const tableName = `${data[i].TableName}`;
        const d = new Date(data[i].ImportTime);
        // ${('0' + d.getDate()).slice(-2)}/${('0' + (d.getMonth() + 1)).slice(-2)}/${d.getFullYear().toString().substring(-2)} - date templates
        const lines = `${numberWithCommas(data[i].Rows)} Rows`;

        importTemps.body.contents.push(
            {
                "type": "box",
                "layout": "baseline",
                "contents": [
                    {
                    "type": "text",
                    "text": tableName,
                    "flex": 1,
                    "color": "#aaaaaa"
                    },
                    {
                    "type": "text",
                    "text": lines,
                    "flex": 1
                    }
                ],
                "margin": "md",
                "flex": 1
            }
        );
    }

    client.push(userID, [
        {
            type: 'flex',
            altText: 'last import data from ITEC',
            contents: importTemps
        }
    ]);
}

async function DateTimeJumping () {
    let message = [];
    await sql.connect(sqlConfig)
    .catch(err => {
        console.log(err);
    });

    await sql.query(`
    DECLARE @DiffDate TABLE (tableName nvarchar(max),FromDate date, ToDate date, diff int);
    DECLARE @allTime TABLE (id int IDENTITY(1,1), tableName nvarchar(max), allDate date);
    DECLARE @tableName nvarchar(max);
    DECLARE @olddate DATE;
    DECLARE @now DATE;
    DECLARE @row int;
    DECLARE @Cursor int = 1;
    DECLARE @cutoff date = '2022-04-01';
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'PO Receipt', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[Buy]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'CN', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[CreditNote]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Customer', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[Customer]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Deposit', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[Deposit]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Inventory In Out', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[InventoryInOut]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Invoice', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[Invoice]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Petty Cash', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[PettyCash]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'PO', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[PO]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Product', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[Product]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Receipt Inv.', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[Receipt]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    INSERT INTO @allTime (tableName, alldate)
    SELECT DISTINCT 'Receipt dep.', CAST([ImportTime] as date) ImportTime
    FROM [ITECToAX].[dbo].[ReceiptDeposit]
    WHERE CAST([ImportTime] as date) >= @cutoff
    ORDER BY CAST([ImportTime] as date) ASC
    
    SELECT @row = COUNT(*) FROM @allTime

        WHILE @row >= @Cursor
            BEGIN
                SELECT @tableName = tableName FROM @allTime
                WHERE id = @Cursor
    
                INSERT INTO @DiffDate (tableName, FromDate, ToDate, diff)
                SELECT @tableName, @olddate, @now, DATEDIFF(DAY, @olddate, @now)
    
                SET @Cursor += 1;
                SET @olddate = @now
                SELECT @now = allDate FROM @allTime WHERE id = @Cursor
            END
    
    SELECT * FROM @DiffDate
    WHERE diff > 1`)
    .then((result) => {
        const data = result.recordset;
       
        for (let i = 0;i < data.length;i++) {
            message.push(`${i + 1}. ${data[i].tableName} From: ${strDate(data[i].FromDate)}; To: ${strDate(data[i].ToDate)}; DIFF: ${data[i].diff}`);
        }

        client.push(userID, [
            {
                type: 'text',
                text: message.join(`\n`)
            }
        ]);
    })
    .catch(err => console.log(err));

    sql.close();
};

async function getNewSubcat() {
    const sqlx = new sqlCommand(configuration);
    var data = await sqlx.queryFromfile('D:\\nodejs\\botAutoAlert\\libs\\sql\\sqllibs\\find_new_subcat.sql');
    var string = "";
    
    if (data.length > 0) {
        for (i = 0;i < data.length;i++) {
            string += `Group category: ${data[i].Detail}(${data[i].GroupCatID})\n`
            string += `Category: ${data[i].CategoryName}(${data[i].CategoryID})\n`
            string += `Sub Category: ${data[i].SubCat}`

            if ((data.length - 1) != i) {
                string += `\n\n`
            }
        }
    }
    
    if (string != "") {
        client.push(userID, [{
            type: 'text',
            text: `New SubCat\n\n${str}`
        }]).catch(err => console.log(err))
    }
}

async function getDocSerial() {
    const d = new Date();
    const Today = `${d.getFullYear()}-${('0' + (d.getMonth() + 1)).slice(-2)}-${('0' + (d.getDate() - 1)).slice(-2)}`
    const sqlx = new sqlCommand(configuration_Temp);
    await sqlx.query(`EXEC [ITECToAXDEV].[dbo].[sp_RegistSerialToDay] @importdate = '${Today}'`).then(async (result) => {
        if (result.length > 0) {
            const sqlx2 = new sqlCommand(configuration);
            await sqlx2.query(`
                EXEC [D365_COM7].[dbo].[SP_importserial] @ImportTime = '${Today}'
            `).then(async (result) => {
                direcrtory = await createExcelfromJsonData(result);
            })
            .catch(err => console.log(err));
        }
    }).catch(err => console.log(err));
}

async function createExcelfromJsonData(data, filename, saveTo = '') {
    const d = new Date();
    const Today = `${d.getFullYear()}${('0' + (d.getMonth() + 1)).slice(-2)}${('0' + (d.getDate() - 1)).slice(-2)}`
    const sheet = wb.addWorksheet('serial 1');
    const header = Object.keys(data[0]);
    const ObjColumns = [];

    for (let i = 0;i < header.length;i++) {
        ObjColumns.push({
            header: header[i],
            key: header[i]
        });
    }

    sheet.columns = ObjColumns;
    sheet.addRows(data);
    if (saveTo != "") {
        saveTo += '/';
    }

    wb.xlsx.writeFile(`./public/worksheets/${saveTo}${filename}_${Today}.xlsx`);

    return `./public/worksheets/${saveTo}${filename}_${Today}.xlsx`
}

async function auto_add_serial(){
    const d = new Date();
    const ToDay = `${d.getFullYear()}${('0' + (d.getMonth() + 1)).slice(-2)}${('0' + (d.getDate() - 1)).slice(-2)}`;
    const sqlx = new sqlCommand(configuration_Temp);
    await sqlx.query(`
        EXEC ITECToAXDEV.dbo.SerialImportv2 @ImportDate = '${ToDay}'
    `).then(async (data) => {
        if (data.length > 0) {
            var sqlz = new sqlCommand(configuration);
            await sqlz.query(`
                USE [D365_COM7]
                EXEC [dbo].[SP_importserial] @ImportTime = '${ToDay}'
            `).then(async (dataz) => {
                    await createExcelfromJsonData(dataz, 'ImportSerial', 'Serial').then((pathFile) => {
                        console.log(pathFile);
                    }).catch(err => console.log(err));
            }).catch(err => console.log(err));
        }
    }).catch(err => {
        console.log(err)
    });
}

(async () => {
    // await getNewSubcat();
    // await getDocSerial();

    await auto_add_serial();
})();

// cron.schedule('1 1 9 * * *', () => {
//     check_daily_import();
//     // DateTimeJumping();
// });